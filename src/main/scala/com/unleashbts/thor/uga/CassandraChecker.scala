package com.unleashbts.thor.uga

import com.datastax.spark.connector._
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.cassandra._
import org.apache.spark.sql.functions._

object CassandraChecker extends App {
  val config: Config = ConfigFactory.load
  val dataPath: String = "data/"

  val conf = new SparkConf(true)
    .setAppName("cassandra_checker")
    .setMaster("local[*]")

  // Set Spark options
  conf.set("spark.driver.maxResultSize", "4g")
  conf.set("spark.executor.memory", "16g")
  conf.set("spark.cassandra.connection.host", config.getString("cassandra.hosts"))
  val spark = SparkSession
    .builder()
    .appName("Cassandra Checker")
    .config(conf)
    .getOrCreate()

  val dataDf = spark.read
    .format("csv")
    .option("header", "true")
    .option("inferSchema", "true")
    .option("delimiter", "\t")
    .load(dataPath + "Results.tsv")
  println(dataDf.distinct().count())

  // // Get count of the Cassandra data table
   val rdd = spark.sparkContext.cassandraTable("uga", "data")
   println(rdd.countApproxDistinct())
}
