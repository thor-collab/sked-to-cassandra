package com.unleashbts.thor.uga

import java.util.UUID

import scala.collection.JavaConversions._
import com.datastax.spark.connector.cql.CassandraConnector
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._


object SkedToCassandra {
  val config: Config = ConfigFactory.load
  val dataPath: String = "data/"
  val keyspaceMap = Map(
    "keyspace" -> "uga",
    "cluster" -> config.getString("cassandra.cluster")
  )
  val experimentMap = Map(
    "E04" -> "AV45la3wk1gXVmrEjJWW"
  )

  def createTableSchema(keyspace: String, name: String): String = {
    val primary = config.getString(s"cassandra.schema.$name.primary")
    val schema = config.getObject(s"cassandra.schema.$name.fields").unwrapped()
    val schemaMap = mapAsScalaMap(schema) map { case (k, v) => s"$k $v" }
    val schemaString = schemaMap.mkString(", ")
    s"CREATE TABLE IF NOT EXISTS $keyspace.$name ($schemaString, PRIMARY KEY ($primary)) WITH compression = {'class': 'LZ4Compressor'}"
  }

  def createMaterializedViewSchema(keyspace: String, baseTable:String, name: String): String = {
    val primary = config.getString(s"cassandra.schema.$name.primary")
    val schema = config.getObject(s"cassandra.schema.$name.fields").unwrapped()
    val cleanPrimary = primary.split(",").map(key => key.stripPrefix("(").stripSuffix(")"))
    val ensureNotNull = cleanPrimary.map(key => s"AND $key IS NOT NULL").mkString(" ").drop(3)
    val schemaMap = mapAsScalaMap(schema) map { case (k, v) => s"$k" }
    val schemaString = schemaMap.mkString(", ")
    s"CREATE MATERIALIZED VIEW $keyspace.$name AS SELECT $schemaString FROM $baseTable WHERE $ensureNotNull PRIMARY KEY ($primary)"
  }

  def calculateReplication(): Int = {
    val hosts = config.getString("cassandra.hosts").split(",")
    if (hosts.length < 3) hosts.length
    else 3
  }

  def lowercaseAllColumns(df: DataFrame): DataFrame = {
    df.columns.foldLeft(df)((curr, n) =>
      curr.withColumnRenamed(n, n.toLowerCase)
    )
  }

  def writeToCassandra(name: String, df: DataFrame): Unit = {
    val cleanedDf = lowercaseAllColumns(df).na.replace("experiment_id", experimentMap)
    cleanedDf.write
      .format("org.apache.spark.sql.cassandra")
      .options(Map("table" -> name) ++ keyspaceMap)
      .save()
  }

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf(true)
      .setAppName("sked_to_cassandra")
      .setMaster("local[*]")

    // Set Spark options
    conf.set("spark.driver.maxResultSize", "4g")
    conf.set("spark.executor.memory", "16g")
    conf.set("spark.cassandra.connection.host", config.getString("cassandra.hosts"))
    val spark = SparkSession
      .builder()
      .appName("SKED to Cassandra")
      .config(conf)
      .getOrCreate()

    // Setup the Cassandra keyspace and tables
    CassandraConnector(conf).withSessionDo { session =>
      config
        .getStringList("cassandra.keyspaces")
        .toList
        .foreach((keyspace) => {
          session.execute(s"CREATE KEYSPACE IF NOT EXISTS $keyspace " +
            s"WITH REPLICATION = { 'class': 'SimpleStrategy', 'replication_factor': ${calculateReplication()} }"
          )
          session.execute(s"${createTableSchema(keyspace, "experiment")}")
          session.execute(s"${createTableSchema(keyspace, "protocol")}")
          session.execute(s"${createTableSchema(keyspace, "aliquot")}")
          session.execute(s"${createTableSchema(keyspace, "subject")}")
          session.execute(s"${createTableSchema(keyspace, "term_ontology_by_protocol_app")}")
          session.execute(s"${createTableSchema(keyspace, "data")}")
        })
    }

    import spark.implicits._

    runJdbcDataset(spark)
    CassandraConnector(conf).withSessionDo { session =>
      config
        .getStringList("cassandra.keyspaces")
        .toList
        .foreach((keyspace) => {
          session.execute(s"${createMaterializedViewSchema(keyspace, "protocol", "protocol_by_experiment")}")
          session.execute(s"${createMaterializedViewSchema(keyspace, "protocol", "protocol_by_experiment_by_subject")}")
        })
    }

  }

  private def createExperiment(spark: SparkSession): DataFrame = {
    val experimentDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Experiments.tsv")

    // Fix the date columns and create the metadata field
    val timestampStart = unix_timestamp(
      experimentDf("START_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampEnd = unix_timestamp(
      experimentDf("END_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampModified = unix_timestamp(
      experimentDf("MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    experimentDf
      .withColumn("START_DATE", timestampStart)
      .withColumn("END_DATE", timestampEnd)
      .withColumn("MODIFICATION_DATE", timestampModified)
  }

  private def createProtocol(spark: SparkSession): DataFrame = {
    val protocolAppDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "ProtocolApplications.tsv")
      .withColumnRenamed("MODIFICATION_DATE", "PROTOCOL_APP_MODIFICATION_DATE")
      .withColumnRenamed("DESCRIPTION", "PROTOCOL_APP_DESCRIPTION")
    val protocolDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Protocols.tsv")
      .withColumnRenamed("MODIFICATION_DATE", "PROTOCOL_MODIFICATION_DATE")
      .withColumnRenamed("DESCRIPTION", "PROTOCOL_DESCRIPTION")

    // Format the date columns to a timestamp
    val timestampProtocolApp = unix_timestamp(
      protocolAppDf("PROTOCOL_APP_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampProtocolAppModified = unix_timestamp(
      protocolAppDf("PROTOCOL_APP_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampProtocolModified = unix_timestamp(
      protocolDf("PROTOCOL_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")

    // Join protocol and protocol app table
    val cleanProtocolDf = protocolDf.withColumn("PROTOCOL_MODIFICATION_DATE", timestampProtocolModified)
    val joinedProtocolDf = protocolAppDf
      .withColumn("PROTOCOL_APP_DATE", timestampProtocolApp)
      .withColumn("PROTOCOL_APP_MODIFICATION_DATE", timestampProtocolAppModified)
      .join(cleanProtocolDf, "PROTOCOL_ID")

    // Merge with the experiment-protocolApp xref table
    val experimentPAXrefDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "PaExperimentXref.tsv")
    experimentPAXrefDf.join(joinedProtocolDf, "PROTOCOL_APP_ID")
  }

  private def createProtocolWithSubject(spark: SparkSession): DataFrame = {
    val protocolDf = createProtocol(spark)

    val aliquotDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Aliquots.tsv")
    val subjectFromAliquotDf = aliquotDf.select("EXPERIMENT_ID", "SUBJECT_ID").distinct()
    protocolDf
      .join(subjectFromAliquotDf, "EXPERIMENT_ID")
      .distinct()
  }

  private def createSubject(spark: SparkSession): DataFrame = {
    val aliquotDf = createAliquot(spark)
    val subjectByExperimentXref = aliquotDf.select("SUBJECT_ID", "EXPERIMENT_ID").distinct()

    val subjectDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Subjects.tsv")
    val subjectByExperimentDf = subjectDf.join(subjectByExperimentXref, "SUBJECT_ID")
    val taxaDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Taxa.tsv")
      .withColumnRenamed("NAME", "TAXON_NAME")
      .withColumnRenamed("PARENT_ID", "TAXON_PARENT_ID")
      .withColumnRenamed("MODIFICATION_DATE", "TAXON_MODIFICATION_DATE")
      .withColumnRenamed("RANK", "TAXON_RANK")
    val joinedSubjectDf = subjectByExperimentDf.join(taxaDf, "TAXON_ID")
    joinedSubjectDf.show()

    // Format the date columns to a timestamp
    val timestampModified = unix_timestamp(
      joinedSubjectDf("MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampBirth = unix_timestamp(
      joinedSubjectDf("DATE_OF_BIRTH"),
      "yyyy-mm-dd"
    ).cast("timestamp")
    val timestampTaxonModified = unix_timestamp(
      joinedSubjectDf("TAXON_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    joinedSubjectDf
      .withColumn("DATE_OF_BIRTH", timestampBirth)
      .withColumn("MODIFICATION_DATE", timestampModified)
      .withColumn("TAXON_MODIFICATION_DATE", timestampTaxonModified)
  }

  private def createAliquot(spark: SparkSession): DataFrame = {
    val subjectDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Subjects.tsv")
      .withColumnRenamed("MODIFICATION_DATE", "SUBJECT_MODIFICATION_DATE")
      .withColumnRenamed("NAME", "SUBJECT_NAME")
      .withColumnRenamed("DESCRIPTION", "SUBJECT_DESCRIPTION")
    val taxaDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Taxa.tsv")
      .withColumnRenamed("NAME", "TAXON_NAME")
      .withColumnRenamed("PARENT_ID", "TAXON_PARENT_ID")
      .withColumnRenamed("MODIFICATION_DATE", "TAXON_MODIFICATION_DATE")
      .withColumnRenamed("RANK", "TAXON_RANK")
    val joinedSubjectDf = subjectDf.join(taxaDf, "TAXON_ID")

    // Create the Aliquot dataframe
    val aliquotDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Aliquots.tsv")
      .withColumnRenamed("NAME", "ALIQUOT_NAME")
      .withColumnRenamed("MODIFICATION_DATE", "ALIQUOT_MODIFICATION_DATE")

    // Format the date columns to a timestamp
    val timestampCreation = unix_timestamp(
      aliquotDf("CREATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampAliquotModified = unix_timestamp(
      aliquotDf("ALIQUOT_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampDateOfBirth = unix_timestamp(
      joinedSubjectDf("DATE_OF_BIRTH"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampSubjectModified = unix_timestamp(
      joinedSubjectDf("SUBJECT_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val timestampTaxonModified = unix_timestamp(
      joinedSubjectDf("TAXON_MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")

    // Join the dataframes
    val cleanJoinedSubjectDf = joinedSubjectDf
      .withColumn("DATE_OF_BIRTH", timestampDateOfBirth)
      .withColumn("SUBJECT_MODIFICATION_DATE", timestampSubjectModified)
      .withColumn("TAXON_MODIFICATION_DATE", timestampTaxonModified)
    aliquotDf
      .withColumn("CREATION_DATE", timestampCreation)
      .withColumn("ALIQUOT_MODIFICATION_DATE", timestampAliquotModified)
      .join(cleanJoinedSubjectDf,"SUBJECT_ID")
  }

  private def createTermOntology(spark: SparkSession): DataFrame = {
    val termOntologyDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "TermOntologies.tsv")
    val timestampModified = unix_timestamp(
      termOntologyDf("MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")
    val cleanTermOntologyDf = termOntologyDf.withColumn("MODIFICATION_DATE", timestampModified)

    // Join the term ontology table with the xref table
    val ontologyXRefDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "OntologyXref.tsv")
    val joinedOntologyDF = ontologyXRefDf.join(cleanTermOntologyDf, "ONTOLOGY_TERM_ID")

    // Join with experiment and PA Xref table
    val experimentPAXrefDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "PaExperimentXref.tsv")
    joinedOntologyDF.join(experimentPAXrefDf, "PROTOCOL_APP_ID")
  }

  private def createData(spark: SparkSession): DataFrame = {
    val aliquotDf = createAliquot(spark).select("ALIQUOT_ID", "SUBJECT_ID", "CREATION_DATE")

    val generateUUID = udf(() => UUID.randomUUID().toString)
    val dataDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "Results.tsv")
      .withColumn("UUID", generateUUID())
    val timestampModified = unix_timestamp(
      dataDf("MODIFICATION_DATE"),
      "dd-MMM-yy"
    ).cast("timestamp")

  // Tie data to aliquot
    val joinedDataDf = dataDf
      .withColumn("MODIFICATION_DATE", timestampModified)
      .join(aliquotDf, "ALIQUOT_ID")

    // Tie data to experiment
    val experimentPAXrefDf = spark.read
      .format("csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .option("delimiter", "\t")
      .load(dataPath + "PaExperimentXref.tsv")

    joinedDataDf.join(experimentPAXrefDf, "PROTOCOL_APP_ID")

  }

  private def runJdbcDataset(spark: SparkSession): Unit = {
    writeToCassandra("experiment", createExperiment(spark))
    writeToCassandra("protocol", createProtocolWithSubject(spark))
    writeToCassandra("aliquot", createAliquot(spark))
    writeToCassandra("subject", createSubject(spark))
    writeToCassandra("term_ontology_by_protocol_app", createTermOntology(spark))
    writeToCassandra("data", createData(spark))
  }
}
