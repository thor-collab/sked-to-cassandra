organization := "com.unleashbts.thor"

name := "SkedToCassandra"

version := "0.1"

scalaVersion := "2.11.11"

scalacOptions := Seq("-unchecked",
                     "-feature",
                     "-deprecation",
                     "-encoding",
                     "utf8")

val sparkVersion = "2.1.0"

resolvers ++= Seq(
  "Apache Releases" at "https://repository.apache.org/content/repositories/releases",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

libraryDependencies ++= Seq(
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.9",
  "com.github.nscala-time" %% "nscala-time" % "2.16.0",
  "com.typesafe" % "config" % "1.3.1",
  "mysql" % "mysql-connector-java" % "6.0.6",
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "com.datastax.spark" %% "spark-cassandra-connector" % "2.0.7",
  "com.datastax.cassandra" % "cassandra-driver-core" % "3.5.0"
)
